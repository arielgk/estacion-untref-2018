import React from 'react';
import { render } from 'react-dom'
import configureStore from './store/configureStore';
import initialState from './store/initialState';
import Root from './components/Root';

import './styles/css/app.css';



// import registerServiceWorker from './registerServiceWorker';


const store = configureStore(initialState)


render(
  <Root store={store} />,
  document.getElementById('root')
)
