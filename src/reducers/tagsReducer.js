import * as types from '../actions/actionTypes';
import initialState from '../store/initialState';

const tags = (state = initialState.tags, action) => {
    switch (action.type) {
        case types.LIST_TAGS_SUCCESS:
            return action.tags
        default:
            return state
    }
}

export default tags;
