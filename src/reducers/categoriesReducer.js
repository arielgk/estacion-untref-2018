import * as types from '../actions/actionTypes';
import initialState from '../store/initialState';

const categories = (state = initialState.categories, action) => {
    switch (action.type) {
        case types.LIST_CATEGORIES:
            return action.categories
        default:
            return state
    }
}

export default categories;
