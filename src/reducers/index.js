import { combineReducers } from 'redux';
import ajaxCallInProgress from './ajaxStatusReducer';
import tags from './tagsReducer';
import shows from './showsReducer';


const rootReducers = combineReducers({
  ajaxCallInProgress,
  tags,
  shows,

});


export default rootReducers;
