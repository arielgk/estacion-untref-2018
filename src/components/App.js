import React, {Component} from 'react';
import {Switch, Route} from 'react-router-dom'
import Home from './Home'
import About from './About'
import Header from './Header';
import Show from './Shows/Show';


class App extends Component {
  render() {
    return (<div className="App">
      <Header/>
      bla
      <Switch>
        <Route exact={true} path="/" component={Home}/>
        <Route path="/about" component={About}/>
          <Route path='/shows/:slug' component={Show}/>


      </Switch>
    </div>);
  }
}

export default App;
