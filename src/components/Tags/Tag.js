import React from 'react';
import PropTypes from 'prop-types';
import {connect} from "react-redux";
import ShowSliderItem from '../Shows/ShowSliderItem';
import Carousel from 'nuka-carousel'

const Tag = ({tag, shows}) => {

  return (<div>
    <h2>{tag.name}</h2>
    <Carousel cellSpacing={0} slideWidth={.75}>
      {shows.map(show => <ShowSliderItem key={'show-' + tag.id + '-' + show.id} show={show}/>)}
    </Carousel>

  </div>)
}

Tag.propTypes = {
  tag: PropTypes.object.isRequired,
  shows: PropTypes.array.isRequired
}

const mapStateToProps = (state, ownProps) => {

  return {
    tag: ownProps.tag,
    shows: state.shows.filter((s) => {
      return s.tags.indexOf(ownProps.tag.id) !== -1;
    })
  }
}

export default connect(mapStateToProps)(Tag);
