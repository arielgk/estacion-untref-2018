import React from 'react';
import PropTypes from 'prop-types';
import {connect} from "react-redux";
import Tag from './Tag';






const Tags = ({ tags}) => (
  <div className="tagsContainer">

    {
           tags.map(t => (
             <Tag key={'tag'+t.id} tag={t} />
           ))
         }


</div>
);


Tags.propTypes = {
  tags:PropTypes.array.isRequired,
}





const mapStateToProps = (state,ownProps)=>{
  return{
    tags:state.tags,
    shows:state.shows
  }

}

export default connect(mapStateToProps)(Tags)
