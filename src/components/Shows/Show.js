import React from 'react';
import {Switch, Route} from 'react-router-dom'
import {connect} from "react-redux";
const Show = ({show}) => (<div >
  <div>{show.name}</div>
</div>)


const mapStateToProps = (state,ownProps)=>{
  console.log(ownProps);
  return {
    show:state.shows.filter( (s)=>{
      return s.id == ownProps.id
    })
  }
}
export default connect(mapStateToProps)(Show);
