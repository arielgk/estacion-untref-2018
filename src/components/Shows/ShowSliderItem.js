import React from 'react';
import {Link} from 'react-router-dom';
// import {connect} from "react-redux";


const ShowSliderItem = ({show}) => (
  <div className="slider-show-item">
    <Link to={`/show/${show.slug}`}>
    <div className="show-title">
      {show.name}
    </div>
    <h2>dfds</h2>
    <figure>
      <img src={show.images.thumb}/>
    </figure>
  </Link>
  </div>
)

export default ShowSliderItem;
