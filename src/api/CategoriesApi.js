const CategoriesAPI = {
  categories: [
    { id: 1, name: "Notas",slug:"notas" },
    { id: 2, name: "Música",slug:"musica" },
    { id: 3, name: "Deportes",slug:"deportes" },
    { id: 4, name: "Audiovisual",slug:"audiovisual" },
    { id: 5, name: "Muntref",slug:"muntref" },
    { id: 6, name: "Ni una menos",slug:"ni una menos" },
    { id: 7, name: "Xirgu",slug:"xirgu" },


  ],
  all: function() { return this.categories},
  get: function(id) {
    const isCategory = p => p.number === id
    return this.categories.find(isCategory)
  }
}

export default CategoriesAPI
