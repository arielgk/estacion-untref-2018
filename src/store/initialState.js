export default {
  // categories: [
  //   {
  //     id: 323,
  //     name: "Cat 1"
  //   }, {
  //     id: 434,
  //     name: "Cat 2"
  //   }
  // ],
  tags: [
    {
      id: 1,
      name: "test-tag-1"
    }, {
      id: 2,
      name: "test-tag-2"
    }, {
      id: 3,
      name: "test-tag-3"
    }, {
      id: 4,
      name: "test-tag-4"
    }
  ],
  shows: [
    {
      id: 22,
      name: "show 1",
      slug:"show-1",
      description: "basdfas fas fs",
      images:{
        thumb:'http://fillmurray.com/200/200',
        large:'http://fillmurray.com/400/500'
      },
      'categories': [323],
      'tags': [1, 2, 4]
    }, {
      id: 33,
      name: "show 2",
      slug:"show-2",
      description: "basdfas fas fs",
      images:{
        thumb:'http://fillmurray.com/200/200',
        large:'http://fillmurray.com/400/500'
      },
      'categories': [434],
      'tags': [1, 3, 4]
    }, {
      id: 44,
      name: "show 3",
      slug:"show-3",
      description: "basdfas fas fs",
      images:{
        thumb:'http://fillmurray.com/200/200',
        large:'http://fillmurray.com/400/500'
      },
      'categories': [
        434, 323
      ],
      'tags': [1, 2, 3]
    }
  ],
  ajaxCallInProgress: 0
}
