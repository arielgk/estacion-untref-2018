import * as types from "./actionTypes";

import axios from 'axios';
import * as config from "../helpers/config";
// import {loadItemsSuccess} from "./itemsActions";

export const  listTags= () => {
    return (dispatch) => {


        axios({
            method:'get',
            url: config.BASE_URL+'/api/tags',
            responseType:'json'
        })
            .then(function(response) {
                dispatch({type: types.LIST_TAGS_SUCCESS,tags:response.data});
            });



    }

}
